#!/usr/bin/env python
# -*- coding: utf-8 -*-


#########################################################################
# This program is a demonstrator on how to use tornado-on-android.      #
# Python commands are entered by users through a custom web page and is #
# interpreted on-the-fly by our custom Python server.... ENJOY !        #
#########################################################################


# Copyright © 2019, Damien André <damien2andre@gmail.com>

# Permission is hereby granted, free of charge, to any person obtaining a copy of this software
# and associated documentation files (the “Software”), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.

# The Software is provided “as is”, without warranty of any kind, express or implied, including but
# not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement.
# In no event shall the authors or copyright holders X be liable for any claim, damages or other liability,
# whether in an action of contract, tort or otherwise, arising from, out of or in connection with the
# software or the use or other dealings in the Software.
# Except as contained in this notice, the name of the Damien André shall not be used in advertising or otherwise
# to promote the sale, use or other dealings in this Software without prior written authorization from the Damien André.





# let's import all what we need
import kivy                                                                                     
from kivy.app import App                                                                        
from kivy.utils import platform                                                                 
from kivy.uix.widget import Widget
from kivy.clock import Clock
from kivy.logger import Logger
from kivy.storage.jsonstore import JsonStore
from kivy.utils import platform

from threading import Thread
from os import getcwd
from tornado import websocket
import json
import tornado.ioloop
import asyncio
import time



# A hack to run and execute a webview on android from python
webView       = None
webViewClient = None
activity      = None


def run_on_ui_thread(arg):
    pass 

if platform == 'android':
    from jnius import autoclass                                                                     
    from android.runnable import run_on_ui_thread                                                   
    webView       = autoclass('android.webkit.WebView')                                                   
    webViewClient = autoclass('android.webkit.WebViewClient')                                       
    activity      = autoclass('org.kivy.android.PythonActivity').mActivity




# web socket server     
class EchoWebSocket(websocket.WebSocketHandler):

    # mandatory for websocket
    def open(self):
        Logger.info("open connection")
        self.send_ok()

        # to store user environments (variable)
        self.global_env = {}
        self.local_env = {}
        
    
    # mandatory for websocket
    def on_message(self, message):
        json_data = json.loads(message)
        Logger.info("receive:" + str(json_data))

        if (json_data['cmd'] == "interpret"):
            # trying to evaluate expression
            try:
                res = str(eval(json_data['content'], self.global_env, self.local_env))
                self.send_msg("The result of your command '{}' is '{}'".format(json_data['content'], res))
                return
            except:
                pass

            # if it don't work trying to exec the expression
            try:
               res = exec(json_data['content'], self.global_env, self.local_env)
               self.send_ok()
            except:
                self.send_msg("Sorry, I can't understand expression '{}'".format(json_data['content']))
            

    # mandatory for websocket       
    def on_close(self):
        Logger.info("close connection")

     # mandatory for websocket, we don't need here any verification here 
     # because it is a local webserver
    def check_origin(self, origin):
        return True

    # custom method, send okay to client
    def send_ok(self):
        time.sleep(1)
        json_data = {}
        json_data['cmd'] = 'ok'
        self.write_message(json.dumps(json_data))
        
    # custom method, send a message to client
    def send_msg(self, msg):
        time.sleep(1)
        json_data = {}
        json_data['cmd'] = 'msg'
        json_data['content'] = msg
        self.write_message(json.dumps(json_data))
        Logger.info("send", json_data)


        

# the webserver must be ran on a separated thread        
class WebServer(Thread):
    def __init__(self):
        super(WebServer, self).__init__()
        
    def run(self):
        Logger.info("Awaiting connection on port 9999")
        asyncio.set_event_loop(asyncio.new_event_loop())
        application = tornado.web.Application([(r'/ws', EchoWebSocket),])
        application.listen(9999)
        tornado.ioloop.IOLoop.instance().start()


# the main kivy widget, it triggers a web browser that points
# on the good webpage : 'minipy.html'
# note that it must be treated separatly if you are under linux/android
class Wv(Widget):                                                                               
    def __init__(self, **kwargs):
        self.f2 = self.create_webview
        super(Wv, self).__init__(**kwargs)
        self.visible = False
        WebServer().start()
        if platform == 'android':
            Clock.schedule_once(self.create_webview, 0)
        else:
            Clock.schedule_once(self.open_web_browser, 0)


    # android case        
    @run_on_ui_thread                                                                           
    def create_webview(self, *args):
        webview = webView(activity)
        webview.getSettings().setJavaScriptEnabled(True)
        wvc = webViewClient();
        webview.setWebViewClient(wvc);
        activity.setContentView(webview)
        file_path = "file:///" + getcwd() + "/html/minipy.html"
        webview.loadUrl(file_path)

    # GNU/linux case    
    def open_web_browser(self, *args):
        import webbrowser
        webbrowser.open_new_tab('./html/minipy.html')
        

# ok, let's go !
class ServiceApp(App):                                                                          
    def build(self):                                                                            
        return Wv()                                                                             


if __name__ == "__main__":
    ServiceApp().run()

