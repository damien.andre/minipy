// Copyright © 2019, Damien André <damien2andre@gmail.com>

// Permission is hereby granted, free of charge, to any person obtaining a copy of this software
// and associated documentation files (the “Software”), to deal in the Software without restriction,
// including without limitation the rights to use, copy, modify, merge, publish, distribute,
// sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in all copies or
// substantial portions of the Software.

// The Software is provided “as is”, without warranty of any kind, express or implied, including but
// not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement.
// In no event shall the authors or copyright holders X be liable for any claim, damages or other liability,
// whether in an action of contract, tort or otherwise, arising from, out of or in connection with the
// software or the use or other dealings in the Software.
// Except as contained in this notice, the name of the Damien André shall not be used in advertising or otherwise
// to promote the sale, use or other dealings in this Software without prior written authorization from the Damien André.


var ws = new WebSocket("ws://127.0.0.1:9999/ws");


$(document).ready(function(){    
    waitingDialog.show('awaiting connection...');
}) 


ws.onmessage = function(e){    
    json = JSON.parse(e.data);

    if(json["cmd"]=="ok") {
	waitingDialog.message("ok")
    }
    
    waitingDialog.hide();

    
    if(json["cmd"]=="msg") {
	waitingDialog.hide();
	$('#modal-msg-content').html(json["content"]);
	$('#modal-msg').modal('show');
    }

}



function run(e) {
    waitingDialog.show('please wait...');
    param = {
	"cmd":"interpret",
	"content": document.getElementById('python-content').value,
    };
    ws.send(JSON.stringify(param));
}




