# minipy

minipy is an android application that allows you for executing short python command on your phone.
In fact, this application is a short demonstrator on how to execute a python server on android. 
The client part is a simple html page. A local communication between client and server is ensured 
through websocket. The source code of minipy is freely distributed under the MIT license.
To get more information about license, please see the LICENSE file.

# Some note about building app 
build the apk in the debug mode, deploy and run it on the device  :
```
$ buildozer android debug deploy run
```
be aware, if you want to make a relase, please do :
```
$ buildozer android release
```
-> Note that the new apk are located in the 'bin' directory


## DEPLOYING APP WITH ADB 

Send and install the apk, on your device 
```
$ adb install -r ./bin/my.apk
```

## CREATING YOUR SIGNING KEY (FOR GG PLAY)
make a 'keystores' directory
```
$ mkdir 'keystores'
```
generate the key with alias 'cb-play'
```
$ keytool -genkey -v -keystore ./keystores/pyrfda.keystore -alias cb-play -keyalg RSA -keysize 2048 -validity 10000
```



## IMPORTANT ! SIGNING APP AND DEPLOYING IT (FOR GG PLAY)
sign the release app
```
$ jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ../keystores/pyrfda.keystore ./bin/my.apk cb-play
```

zip align the app
```
$ ~/.buildozer/android/platform/android-sdk-20/build-tools/23.0.1/zipalign -v 4 ./bin/my.apk ./bin/my-release.apk
```

send the apk to the device
```
$ adb install -r ./bin/my-release.apk
```


